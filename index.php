<?php
	require_once 'assets/php/Mobile_Detect.php';
	$detect = new Mobile_Detect;
	$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'desktop');
	$scriptVersion = $detect->getScriptVersion();
?>
<!doctype html>
<!--[if lte IE 9]><html class="lteIE9 loading"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="loading"><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Welcome to the online portfolio for Rick Reyes, a senior level front-end web developer, graphic designer and digital audio specialist located in Dallas, Texas.">
        <title>Rick Reyes | Online Portfolio</title>
        <link rel="shortcut icon" href="/assets/img/favicon.png">
        <style><?php include ('assets/css/site.css'); ?></style>
    </head>
    <body class="<?php echo $deviceType; ?>">
        <?php include ('assets/inc/header.php'); ?>
        <?php include ('assets/inc/contact.php'); ?>
        <?php include ('assets/inc/web.php'); ?>
        <?php include ('assets/inc/print.php'); ?>
        <?php include ('assets/inc/footer.php'); ?>
        <?php include ('assets/inc/scripts.php'); ?>
    </body>
</html>