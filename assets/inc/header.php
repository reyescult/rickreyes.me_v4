<header class="site">
    <div class="cnt">
        <h1>Rick Reyes</h1>
        <div class="contact_menu">
            <a href="assets/doc/rick_reyes_2019_resume.pdf" rel="nofollow"><span>Download </span>Resume</a>
            <a class="send_email" rel="nofollow"><span>Send </span>Email</a>
            <a href="tel:1-214-253-9059" rel="nofollow">Call<span> Now</span></a>
        </div>
    </div>
</header>