<footer>
    <div class="cnt">
        <div class="social">
            <a href="https://www.facebook.com/reyescult" rel="nofollow">Facebook</a>
            <a href="https://www.google.com/+RickReyes1972" rel="nofollow">Google+</a>
            <a href="https://soundcloud.com/reyescult" rel="nofollow">SoundCloud</a>
            <a href="http://lnkd.in/T_wU9R" rel="nofollow">LinkedIn</a>
        </div>
        <div class="copyright">
            <p>Mobile-First Website by Reyescult DMD.</p>
            <p>Copyright &copy;2015-<span class="cntYear"></span> Reyescult DMD. All Rights Reserved.</p>
        </div>
    </div>
</footer>