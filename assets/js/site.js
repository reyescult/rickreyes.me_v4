/*global $, jQuery*/

(function ($) {
    
    "use strict";
    
    /* Window Load */
    
    $(window).load(function () {
        
        // display message if lower than ie10
            
        if ($('html').hasClass('lteIE9')) {

            $('body').html('<p class="ieMsg">This site is build using HTML5 &amp; CSS3 that is not fully supported in Internet Explorer 9 and below.<br /><a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie">Click Here</a> to upgrade to the latest version of Internet Explorer.</p>');

        }
        
        // remove 'loading' class after page load
        
        setTimeout(function () {
            
            $('html').removeClass('loading');
            
        }, 50);
        
        // bind responsive slider after page load

        setTimeout(function () {

            $('div.slider').mySlider();

        }, 50);
        
    });

	// Disable CSS Transitions on Resize

	function doneResizing() {

        $('body').removeClass('noTransitions');

	}

	var onResizeEnd;

	$(window).resize(function () {

		clearTimeout(onResizeEnd);

		onResizeEnd = setTimeout(doneResizing, 50);

		$('body').addClass('noTransitions');

	});
    
    /* Toggle Contact Form */
    
    $('a.send_email').click(function () {
        
        $(this).toggleClass('on');
        
        $('section.contact').toggleClass('on');
        
    });

    /* Toggle Samples */

    $('div.caption button').click(function () {

        var toggleText = $(this).html() === 'Hide Samples' ? 'View Samples' : 'Hide Samples';
        
        $(this).html(toggleText).parents().siblings('div.samples').toggleClass('on');

    });
    
    /*  Modal Dialog */
    
    $('[class*="slide_"] a').click(function (e) {
        
        e.preventDefault();
        
        var href = $(this).attr('href'),
            desc = $(this).html();
        
        $.get('assets/inc/modal.php', function (modal) {
            
            // append modal to body
            
            $(modal).css({
                
                opacity: '0'
            
            }).appendTo('body');
            
            // update modal content
            
            $('h3.mTitle').html(desc).siblings('img.mImage').attr('src', href);
            
            // show modal
            
            $('div.modal').animate({
                
                opacity: '1'
                
            }, 500).parent('body').css({
                
                overflow: 'hidden'
                
            });
            
            // destroy modal
            
            $('div.modal').click(function (e) {
                
                if ($(e.target).is('a.mClose')) {
                    
                    $(this).animate({

                        opacity: '0'

                    }, 500, function () {
                        
                        $(this).remove();
            
                        $('body').css({

                            overflow: 'auto'

                        });
                        
                    });
                    
                }
                
            });
            
        });
        
    });
    
    /* Social Links */
    
    $('div.social a').click(function (e) {
        
        window.open(this.href);
        
        e.preventDefault();
        
    });

    /* Current Year */

    $('span.cntYear').text((new Date()).getFullYear());
    
}(jQuery));