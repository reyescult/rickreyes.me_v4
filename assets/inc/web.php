<section class="web section">
    <div class="web summary">
        <div class="cnt">
            <h2><span>Web</span></h2>
            <div class="caption">
                <p>With over 12 years professional front-end development and UI design experience, you can count on pixel accurate layout and strict adherence to web standards.</p>
                <p>I specialize in mobile-first front-end development using industry standard HTML5, CSS3 and Javascript. As a result, I am able to deliver a strealined UI framework architecture that will respond and adapt to all major device types and platforms.</p>
                <button type="button">View Samples</button>
            </div>
        </div>
    </div>
    <div class="web samples">
        <div class="cnt">
            <div class="slider">
                <ul class="slides">
                    <li class="slide_01 active">
                        <a href="/assets/img/web/desktop/flm.jpg" class="capone-flm">Capital One Microsite</a>
                        <a href="/assets/img/web/desktop/auto-combo.png" class="capone-auto-a">Auto Navigator A</a>
                        <a href="/assets/img/web/desktop/auto-landing-page.jpg" class="capone-auto-b">Auto Navigator B</a>
                        <a href="/assets/img/web/desktop/dealer-combo.png" class="capone-dealer">Dealer Navigator</a>
                    </li>
                    <li class="slide_02">
                        <a href="/assets/img/web/desktop/sas.png" class="sas">Solid Accounting Solutions</a>
                        <a href="/assets/img/web/desktop/vv-exam-room.png" class="soma">Verizon Virtual Visits</a>
                        <a href="/assets/img/web/desktop/offshoot.jpg" class="offshoot">Doctorlogic Offshoot</a>
                        <a href="/assets/img/web/desktop/reactive.jpg" class="reactive">Doctorlogic Reactive</a>
                    </li>
                    <li class="slide_03">
                        <a href="/assets/img/web/desktop/skyline.jpg" class="skyline">Doctorlogic Skyline</a>
                        <a href="/assets/img/web/desktop/metro.jpg" class="metro">Doctorlogic Metro</a>
                        <a href="/assets/img/web/desktop/elegant.jpg" class="elegant">Doctorlogic Elegant</a>
                        <a href="/assets/img/web/desktop/rrmV3.png" class="rrmV3">rickreyes.me Version 3.0</a>
                    </li>
                    <li class="slide_04">
                        <a href="/assets/img/web/desktop/skin12a.jpg" class="skin12a">iMatrix Skin 12a</a>
                        <a href="/assets/img/web/desktop/skin14a.jpg" class="skin14a">iMatrix Skin 14a</a>
                        <a href="/assets/img/web/desktop/dentalmatrix.jpg" class="dentalmatrix">DentalMatrix</a>
                        <a href="/assets/img/web/desktop/icontrol.png" class="icontrol">iMatrix iControl CMS</a>
                    </li>
                    <li class="slide_05">
                        <a href="/assets/img/web/desktop/frame-selector.png" class="frame-selector">iMatrix Frame Selector</a>
                        <a href="/assets/img/web/desktop/irefer.png" class="irefer">DentalMatrix iRefer</a>
                        <a href="/assets/img/web/desktop/chiromatrix-v2.jpg" class="chiromatrix-v2">ChiroMatrix V2</a>
                        <a href="/assets/img/web/desktop/prosport.jpg" class="prosport">ChiroMatrix ProSport</a>
                    </li>
                    <!--<li class="slide_06">
                        <a href="/assets/img/web/desktop/chiromatrix.png" class="chiromatrix">ChiroMatrix</a>
                        <a href="/assets/img/web/desktop/skin09a.png" class="skin09a">iMatrix Skin09a</a>
                        <a href="/assets/img/web/desktop/beyond-wellness.jpg" class="beyond">Beyond Wellness</a>
                        <a href="/assets/img/web/desktop/living-well.png" class="living">Living Well New York</a>
                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</section>