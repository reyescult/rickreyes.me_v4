/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */

/*global define, $, jQuery*/

(function ($) {

    "use strict";

    $.fn.mySlider = function (options) {

        // plugin settings

        var settings = $.extend({

            slideSpeed: 1000, // default slide speed
            slideName: 'Slide' // default slide indicator link text

        }, options);

        // plugin function

        return this.each(function () {

            // slider settings

            var slider = $(this),
                sliderWidth = slider.outerWidth(),
                slideParent = slider.find('.slides'),
                slide = slideParent.find('[class*="slide_"]'),
                slideCount = slide.length,
                slideParentWidth = sliderWidth * slideCount,
                slideIndicators = $('<div />', {'class': 'slide_indicators'});

            // adjust slider widths on window resize

            $(window).resize(function () {

                sliderWidth = slider.outerWidth();
                slideParentWidth = sliderWidth * slideCount;

                slideParent.outerWidth(slideParentWidth).children().outerWidth(sliderWidth);

            }).trigger('resize');

            // append indicators container to slider

            slideIndicators.appendTo(slider);

            // slide function

            slide.each(function () {

                // slide settings

                var slide = $(this),
                    slideClass = slide.attr('class').split(' ')[0],
                    slideIndex = slide.index() + 1,
                    slideIndicator = '<a href="#" class="' + slideClass + '">' + settings.slideName + ' ' + slideIndex + '</a>',
                    slideIndicatorParent = slide.parent().next('.slide_indicators');

                var slidePosition = slide.position();

                // adjust slide parent position on window resize

                $(window).resize(function () {

                    slidePosition = slide.position();

                    if (slide.hasClass('active')) {

                        slideParent.css('left', -slidePosition.left);

                    }

                });

                // append slide indicators to indicator container

                $(slideIndicator).appendTo(slideIndicatorParent);

                if (slide.hasClass('active')) {

                    slideParent.css('left', -slidePosition.left);

                    $('a.' + slideClass).addClass('active');

                }

                // slide indicator click function

                $('a.' + slideClass).click(function (e) {

                    var indicator = $(this),
                        indicatorIndex = indicator.index(),
                        indicatorSlide = indicator.parent().parent().find('[class*="slide_"]').eq(indicatorIndex),
                        indicatorSlideParent = indicator.parent().siblings('.slides');

                    // prevent default action

                    e.preventDefault();

                    // set slide indicator and slide classes

                    if (!indicator.hasClass('active')) {

                        indicator.add(indicatorSlide).addClass('active').siblings().removeClass('active');

                    }

                    // adjust slide parent position

                    if (slide.hasClass('active')) {

                        indicatorSlideParent.stop().animate({

                            left: -slidePosition.left

                        }, settings.slideSpeed);

                    }

                });

            });

            // show slider when ready

            slider.css('visibility', 'visible');

        });

    };

}(jQuery));