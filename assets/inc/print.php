<section class="print section">
    <div class="print summary">
        <div class="cnt">
            <h2><span>Print</span></h2>
            <div class="caption">
                <p>My background in graphic design for print spans over 20 years and includes a wide variety of projects that were taken from initial concept to finished product.</p>
                <h4>Projects include:</h4>
                <ul>
                    <li>128 Page Perfect Bind magazine</li>
                    <li>Printer friendly SEO user guide</li>
                    <li>Full Hummer H2 vehicle wrap</li>
                    <li>Multiple trade show displays</li>
                    <li>Large format banners and posters</li>
                    <li>Post cards, business cards, letterhead and more</li>
                </ul>
                <button type="button">View Samples</button>
            </div>
        </div>
    </div>
    <div class="print samples">
        <div class="cnt">
            <div class="slider">
                <ul class="slides">
                    <li class="slide_01 active">
                        <a href="/assets/img/print/desktop/sas-card.png" class="sas">SAS Business Cards</a>
                        <a href="/assets/img/print/desktop/seo-guide.jpg" class="seo">ChiroMatrix SEO User Guide</a>
                        <a href="/assets/img/print/desktop/gen3-postcard.jpg" class="gen3">ChiroMatrix Gen3 Postcard</a>
                        <a href="/assets/img/print/desktop/success.jpg" class="success">Helmets To Hardhats Success Magazine</a>
                    </li>
                    <li class="slide_02">
                        <a href="/assets/img/print/desktop/vet-prescription-ad.jpg" class="prescription">VetMatrix Prescription Ad</a>
                        <a href="/assets/img/print/desktop/florida-guard-poster.jpg" class="fng">Hire A Hereo Florida Guardsmen Poster</a>
                        <a href="/assets/img/print/desktop/hummer.jpg" class="hummer">Helmets To Hardhats Hummer H2</a>
                        <a href="/assets/img/print/desktop/vivify.jpg" class="vivify">Vivify Designs Postcard</a>
                    </li>
                    <li class="slide_03">
                        <a href="/assets/img/print/desktop/dcp-ad.png" class="dcp">ChiroMatrix DCP Ad</a>
                        <a href="/assets/img/print/desktop/pocket-card.jpg" class="pcard">Hire A Here Pocket Card</a>
                        <a href="/assets/img/print/desktop/scholarship-ad.jpg" class="scholarship">Hire A Here Scholarship Ad</a>
                        <a href="/assets/img/print/desktop/bravura.jpg" class="bravura">Bravura Networks Ad &amp; Business Card</a>
                    </li>
                    <li class="slide_04">
                        <a href="/assets/img/print/desktop/o22-card.png" class="o22">Opulent22 Business Card</a>
                        <a href="/assets/img/print/desktop/reyescult-card.png" class="reyescult">Reyescult Business Card</a>
                        <a href="/assets/img/print/desktop/ironworkers.png" class="ironworkers">Helmets To Hardhats Ironworkers Sticker</a>
                        <a href="/assets/img/print/desktop/write-institute.png" class="write">Write Institute Learning Aids</a>
                    </li>
                    <li class="slide_05">
                        <a href="/assets/img/print/desktop/bp-time.jpg" class="pillow">Blanket &amp; Pillow Time</a>
                        <a href="/assets/img/print/desktop/martian-holidy.jpg" class="martian">Martian Holiday</a>
                        <a href="/assets/img/print/desktop/brotherhood.png" class="brotherhood">Helmets To Hardhats Brotherhood Sticker</a>
                        <a href="/assets/img/print/desktop/american-hero.jpg" class="hero">Helmets To Hardhats American Hero Award</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>